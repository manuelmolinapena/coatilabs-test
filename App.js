const Express = require('./lib/Express');
const express = require('express');
const Router = require('./routes/Router');
const app = express();
class App {
    static async Load() {
        try {
            new Express(app);
            new Router(app);

            return app;
        } catch (error) {
            console.log(error)   
        }
    }

}

module.exports = App;
