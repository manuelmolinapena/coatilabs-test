global.config = require('./config.json');

const chalk = require('chalk');
const App = require('./App');
const http = require('http');
const port = global.config.server.port;

// eslint-disable-next-line no-console
const log = console.log;

function run() {
    App.Load().then((app) => {
        const server = http.createServer(app);
        const onListening = () => log(chalk.green.bgBlack(`Listening on http://localhost:${port}`));

        server.listen(port);
        server.on('error', onError);
        server.on('listening', onListening);

        function onError(error) {
            if (error.syscall !== 'listen') throw error;
            const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

            switch (error.code) {
                case 'EACCES':
                    log(bind + ' requires elevated privileges');
                    break;
                case 'EADDRINUSE':
                    log(bind + ' is already in use');
            }

            throw error;
        }
    }).catch((error) => {
        log(chalk.red.bgBlack(error.message));
        process.exit(1);
    });
}

run();
