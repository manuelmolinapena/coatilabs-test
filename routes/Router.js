const UserController = require('../app/APIController/UserController');

const v1 = '/api/v1';

class Router {
    constructor(app) {
        new UserController(app).Router(`${v1}/user`);
    }
}

module.exports = Router;