const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const shortid = require('shortid');

class Db {
    constructor(model){
        this.model = model;
        const adapter = new FileSync(`${model}db.json`);
        const db = low(adapter);

         db.defaults( {users:[]})
            .write()

        this.db = db;
    }

    async find(condition = {removed: false}, limit = 10 ){
        try {
            return this.db.get(this.model)
                .filter(condition)
                .take(limit)
                .value();
        } catch (error) {
            return error;
        }
    }
    
    async count(condition = null){
        try {
            if(condition)
                return (this.db.get(this.model).filter(condition).value()).length;
            else
                return (this.db.get(this.model).value()).length;
        } catch (error) {
            return error;
        }
    }

    async findOne(condition = {}){
        try {
            return this.db.get(this.model)
                .find(condition)
                .value()
        } catch (error) {
            return error;
        }
    }

    async findOneAndUpdate(condition = {}, value = {}){
        try {
            return this.db.get(this.model)
                .find(condition)
                .assign(value)
                .write();
        } catch (error) {
            return error;
        }   
    }

    async create(item){
        try {
            item.id = shortid.generate();
            return this.db.get(this.model)
                .push(item)
                .write()
        } catch (error) {
            return error;
        }
    }

    async remove(id = {}){
        try {
            return this.db.get(this.model)
                .find(id)
                .assign({removed: true})
                .write();
        } catch (error) {
            return error;
        }
    }
}

module.exports = Db