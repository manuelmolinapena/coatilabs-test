const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('morgan');

class Express {
    constructor(app) {
        this.app = app;
        this.initialize();
    }

    initialize() {
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(cors());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(helmet());
    }
}

module.exports = Express;