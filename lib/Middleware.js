const jwt = require('jwt-simple');

class Middelware {
    static VerifyToken(req) {
        const [ , token ] = req.headers.authorization.split(' ');
        req.user = jwt.decode(token, global.config.sessionSecret);
    }

    static Auth(req, res, next) {
        try {
            Middelware.VerifyToken(req);
            next();
        } catch (error) {
            return res.status(401).json({ error: true, message: 'Invalid access.' });
        }
    }

    static IsAdmin(req, res, next) {
        try {
            Middelware.VerifyToken(req);
            if (req.user.role !== 'administrator') throw new Error();
            next();
        } catch (error) {
            return res.status(401).json({ error: true, message: 'Invalid access.' });
        }
    }

    static IsUser(req, res, next) {
        try {
            Middelware.VerifyToken(req);
            if (req.user.role !== 'user' && req.user.role !== 'administrator') throw new Error();
            next();
        } catch (error) {
            return res.status(401).json({ error: true, message: 'Invalid access.' });
        }
    }
}

module.exports = Middelware;