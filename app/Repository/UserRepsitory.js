const bcrypt = require('bcryptjs');
const BaseCrudRepository = require('./BaseCrudRepository');

class UserReposiroty extends BaseCrudRepository{
    constructor(){
        super('users');
    }

    async Create(record){
        record.password = bcrypt.hashSync(record.password, bcrypt.genSaltSync(8));
        return super.Create(record);
    }

    async FindOneAndUpdate(id,record){
        record.password = bcrypt.hashSync(record.password, bcrypt.genSaltSync(8));
        return super.FindOneAndUpdate(id,  record);
    }

}

module.exports = UserReposiroty;