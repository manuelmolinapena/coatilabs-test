const Db = require('../../lib/db/DB');

class BaseCrudRepository {
    constructor(model) {
        this.name = model;
        this.db = new Db(model);
    }

    async FindAll(condition = {}, limit = null) {
        return this.db.find(condition, limit);
    }

    async Count(condition = null) {
        return this.db.count(condition);
    }

    async Find(condition = {}) {
        return this.db.findOne(condition)
    }

    async FindOneAndUpdate(condition = {}, data = {}) {
        return this.db.findOneAndUpdate(condition, data);
    }

    async Create(record) {
        return this.db.create(record);
    }

    async Remove(id) {
        return this.db.remove(id);
    }
}

module.exports = BaseCrudRepository;