const IApiController = require('./IApiContoller');
const jwt = require('jwt-simple');
const UserRepository = require('../Repository/UserRepsitory');
const bcrypt = require('bcryptjs');
const middleware = require('../../lib/Middleware');

class UserController extends IApiController {
    constructor($app) {
        super($app);
        this.repository = new UserRepository();

        this.Login = this.Login.bind(this);
        this.Create = this.Create.bind(this);
        this.Delete = this.Delete.bind(this);
        this.Update = this.Update.bind(this);
        this.List = this.List.bind(this);

    }

    Router(path) {
        this.app.post(`${path}/login`, this.Login);
        this.app.post(`${path}/create`, middleware.IsAdmin, this.Create);
        this.app.delete(`${path}/delete/:id`, middleware.IsAdmin, this.Delete);
        this.app.put(`${path}/update`, middleware.IsAdmin, this.Update);
        this.app.get(`${path}/list`, middleware.IsUser, this.List);
    }

    async Login(req, res) {
        try {
            const { name, password } = req.body;
            const user = await this.repository.Find({ name });
            if(!user) throw new Error

            const valid = bcrypt.compareSync(password, user.password);

            if(!valid) throw new Error('Invalid credentials');

            const data = { id: user.id, name: user.name, role: user.role };
            const token = jwt.encode(data, global.config.sessionSecret);
            return this.response(res, { token, user: data });

        } catch (error) {
            return this.response(res, { message: 'Invalid credentials.' }, true, 403);
        }
    }

    async Create(req, res) {
        try {

            if(req.body.role == "user" || req.body.role == "administrator");
            else throw new Error;
            const usr = await this.repository.Find({name: req.body.name, removed: false});
            if(usr) throw new Error(1);

            let user = {
                name: req.body.name,
                password: req.body.password,
                role: req.body.role,
                removed: false,
            }

            await this.repository.Create(user);

            return this.response(res, { message: 'User created successfully.' });
        } catch (error) {
            if(error.message == 1) return this.response(res, { message: 'Name already in use.' }, true, 200);
            else return this.response(res, { message: 'Error creating user.' }, true, 200);
        }
    }

    async Delete(req, res) {
        try {
            const id = req.params.id;
            await this.repository.Remove({id: id});
            return this.response(res, { message: 'User removed successfully.' });
        } catch (error) {
            this.response(res, { message: 'Error removing user.' }, true);
        }
    }

    async Update(req, res) {
        try {
            const { id, name, password, role } = req.body;

            if(role == "user" || role == "administrator");
            else throw new Error;
            
            const usr = await this.repository.Find({name: req.body.name, removed: false});
            if(usr.id != id) throw new Error(1);

            await this.repository.FindOneAndUpdate({id}, { name, password, role });
            return this.response(res, { message: 'User updated successfully.' });

        } catch (error) {
            if(error.message == 1) return this.response(res, { message: 'Name already in use.' }, true, 200);
            else return this.response(res, { message: 'Error updating user.' }, true, 200);
        }
    }

    async List(req, res) {
        try {
            const [userCount, userList] = await Promise.all([
                this.repository.Count({ removed: false}),
                this.repository.FindAll({ removed: false}, 1000)
            ]);

            return this.response(res, { message: 'Get users successfully.', data: { total: userCount, users: userList } });
        } catch (error) {
            this.response(res, { message: 'Error loading users.' }, true);
        }
    }

}

module.exports = UserController;