class IApiController {

    constructor($app) {
        this.app = $app;
    }

    /**
     * 
     * @param {Express.Response} response 
     * @param {object} data 
     * @param {boolean} error 
     * @param {number} codeError 
     */
    response(response, data, error = false, codeError = 500) {
        const code = error ? codeError : 200;
        return response.status(code).json({ error, ...data });
    }
}

module.exports = IApiController;